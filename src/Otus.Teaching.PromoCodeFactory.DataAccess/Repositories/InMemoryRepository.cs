﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; private set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new List<T>(data);
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> AnyAsync(Guid id)
        {
            return Task.FromResult(Data.Any(x => x.Id == id));
        }

        public Task AddAsync(T entity)
        {
            if (Data.Any(x => x.Id == entity.Id))
            {
                throw new NotSupportedException("С данным ключом значение добавлно ранее");
            }

            Data.Add(entity);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            if (!Data.Any(x => x.Id == entity.Id))
            {
                throw new NotSupportedException("С данным ключом значение не найдено");
            }

            var item = Data.FirstOrDefault(x => x.Id == entity.Id);
            var index = Data.IndexOf(item);

            Data.Remove(item);
            Data.Insert(index, entity);

            return Task.CompletedTask;
        }

        public Task RemoveAsync(Guid id)
        {
            Data.RemoveAll(x => x.Id == id);
            return Task.CompletedTask;
        }
    }
}