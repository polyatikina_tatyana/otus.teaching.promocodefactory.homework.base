﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<ActionResult> AddAsync([FromBody] Employee employee)
        {
            if (await _employeeRepository.AnyAsync(employee.Id))
            {
                return Conflict();
            }

            try
            {
                await SaveRolesAsync(employee.Roles);
                await _employeeRepository.AddAsync(employee);
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("update")]
        public async Task<ActionResult> UpdateAsync([FromBody] Employee employee)
        {
            if (!await _employeeRepository.AnyAsync(employee.Id))
            {
                return NotFound();
            }

            try
            {
                await SaveRolesAsync(employee.Roles);
                await _employeeRepository.UpdateAsync(employee);
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!await _employeeRepository.AnyAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _employeeRepository.RemoveAsync(id);
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }

        private async Task SaveRolesAsync(List<Role> roles)
        {
            if ((roles?.Any()) != true)
            {
                return;
            }

            foreach (var role in roles)
            {
                if (!await _rolesRepository.AnyAsync(role.Id))
                {
                    await _rolesRepository.AddAsync(role);
                }
                else
                {
                    await _rolesRepository.UpdateAsync(role);
                }
            }
        }
    }
}