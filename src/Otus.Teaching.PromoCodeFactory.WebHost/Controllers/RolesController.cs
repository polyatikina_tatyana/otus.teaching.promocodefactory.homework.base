﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x =>
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<ActionResult> AddAsync([FromBody] Role role)
        {
            if (await _rolesRepository.AnyAsync(role.Id))
            {
                return Conflict();
            }

            try
            {
                await _rolesRepository.AddAsync(role);
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <returns></returns>
        [HttpPut("update")]
        public async Task<ActionResult> UpdateAsync([FromBody] Role role)
        {
            if (!await _rolesRepository.AnyAsync(role.Id))
            {
                return NotFound();
            }

            try
            {
                await _rolesRepository.UpdateAsync(role);
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!await _rolesRepository.AnyAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _rolesRepository.RemoveAsync(id);
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}